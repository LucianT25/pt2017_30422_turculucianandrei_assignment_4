package entities;
import java.io.Serializable;
import java.util.UUID;

import interfaces.Observer;
import interfaces.Subject;

public abstract class Account implements Subject, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected UUID accountId;
	protected int clientId;
	protected double balance;
	protected Observer owner;
	
	public Account(int cId, double bal, Observer o) {
		this.accountId = UUID.randomUUID();
		this.clientId = cId;
		this.balance = bal;
		this.owner = o;
	}

	public Observer getOwner() {
		return owner;
	}

	public void setOwner(Observer owner) {
		this.owner = owner;
	}
	
	@Override
	public void notifyObserver(Double change) {
		this.owner.update(this, change);		
	}

	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		else if (obj == this) return true;
        
        if (obj.getClass() != this.getClass()) return false;
        else {
		    Account a = (Account) obj;
		    return (this.accountId == a.accountId) && (this.clientId == a.clientId);
        }
    }
	
	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public UUID getAccountId() {
		return accountId;
	}

	public double deposit(double sum){
		return 0;
	}
	public double withdraw(double sum){
		return 0;
	}
	@Override
	public String toString() {
		return "accountId=" + accountId + ", owner="+ clientId + ", balance=" + balance + ", type=" + this.getClass().getSimpleName();
	}

	public void addInterest() {
	}
	
	
	
}
