package entities;
import java.io.Serializable;

import interfaces.Observer;

public class Person implements Observer, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private double savingBalance;
	private double spendingBalance;
	
	public Person(int id, String name) {
		this.id = id;
		this.name = name;
		this.savingBalance = 0;
		this.spendingBalance = 0;
	}
	
	@Override
	public int hashCode() {
		return this.id;
	}
	
	@Override
	public void update(Account account, Double change) {
		if(account.getClass().equals(SavingAccount.class)) {
			this.savingBalance += change;
		} else if(account.getClass().equals(SpendingAccount.class)) {
			this.spendingBalance += change;
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		else if (obj == this) return true;
        
        if (obj.getClass() != this.getClass()) return false;
        else {
		    Person p = (Person) obj;
		    return (this.name.equals(p.name)) && (this.id == p.id);
        }
    }
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSavingBalance() {
		return savingBalance;
	}

	public void setSavingBalance(double savingBalance) {
		this.savingBalance = savingBalance;
	}

	public double getSpendingBalance() {
		return spendingBalance;
	}

	public void setSpendingBalance(double spendingBalance) {
		this.spendingBalance = spendingBalance;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + "]";
	}
	
}
