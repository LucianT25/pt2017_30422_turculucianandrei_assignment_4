package interfaces;
import java.util.UUID;

import entities.Account;
import entities.Person;

public interface BankProc {
	
//Person
/**
 * @Pre client nonexistent
 * @Post client added
 */
public void addPerson(Person p);

/**
 * @Pre client exists, nrOfClients != 0
 * @Post client is removed, nrOfClients--
 */
public void removePerson(Person p);

/**
 * @Pre client exists
 * @Post name updated
 */
public void updatePerson(Person p, String name);
public Person searchClientById(int id);

//Account
/**
 * @Pre person, account nonexistent
 * @Post account added
 */
public void addAccount(Account a, Person p);

/**
 * @Pre existing account
 * @Post account removed
 */
public void removeAccount(Account a);


public void addInterest();
public Account searchAccountById(Person p, UUID id);

//Presentation
public String[][] outputAccountsOfClient(Person p);
public String[][] outputClients();
public void generateReport();

//Validation
public boolean isWellFormed();
}
