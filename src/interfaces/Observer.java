package interfaces;
import entities.Account;

public interface Observer {
	public void update(Account a, Double change);
	
}
